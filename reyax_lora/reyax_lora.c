//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_usart.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <delay.h>
#include <uart.h>
#include <reyax_lora.h>

typedef struct
{
  uint8_t AnswerWaiting         :1;     // ��� �������� ������ �� �������
  uint8_t AnswerReceived        :1;     // ����� �� ������� �������
  uint8_t ErrorReceived         :1;     // ��� ������ �������
  uint8_t ErrorCode;                    // ��� ������, ������� ������ ������ � ����� �� ������
  char *pAnswerBuff;
  char AnswerWord[16];                  // ��������� ����� � ������
}
tLoraState;

static LoraEventFunc EventFunc = 0;  // ��������� �� �������, ���������� �� ������� ������ (�������� �����) 
static tLoraState Reyax_Lora_State;
static char StrBuff[33];


//==============================================================================
// ������� �������� ������ �� UART (�� �������� ��������� ������)
//==============================================================================
static int8_t SendStr(char *pBuff)
{
  uint16_t Len = strlen(pBuff);
  return UART_Send((uint8_t *)pBuff, Len);
}
//==============================================================================


//==============================================================================
// ������� ����� ������ �� �������
//==============================================================================
static int8_t RecvAnswer(char *pAnswer, char *pRecvBuff, uint32_t TimeOut)
{
  Reyax_Lora_State.AnswerWaiting = 1;
  Reyax_Lora_State.pAnswerBuff = pRecvBuff;
  strcpy(Reyax_Lora_State.AnswerWord, pAnswer);
  
  while (TimeOut)
  {
    if (Reyax_Lora_State.ErrorReceived)         // ��� ������ ������
      return Reyax_Lora_State.ErrorCode;
    else if (Reyax_Lora_State.AnswerReceived)   // ����� ������
    {
      Reyax_Lora_State.AnswerReceived = 0;
      return UART_ERR_OK;
    }
    
    TimeOut--;
    delay_ms(1);
  }

  Reyax_Lora_State.AnswerWaiting = 0;
  Reyax_Lora_State.AnswerWord[0] = 0;
  return REYAX_ERR_NO_ANSVER;
}
//==============================================================================


//==============================================================================
// ��������� ������� ��������� ������ �� ������
//==============================================================================
static void RxRFPacket(char *pBuff, uint16_t Len)
{
  // ���������� ������� ��������� �������?
  if (EventFunc)
  {
    // ������ ������
    pBuff += 4;
    Len -= 4;
      
    uint16_t RxAddress = atoi(pBuff); 

    char *pPos = strstr(pBuff, ",");
    int16_t Pos = pPos - pBuff;
    pBuff += (Pos + 1);
    Len -= (Pos + 1);
      
    uint16_t RxLen = atoi(pBuff); 
    
    pPos = strstr(pBuff, ",");
    Pos = pPos - pBuff;
    pBuff += (Pos + 1);
    Len -= (Pos + 1);

    pPos = pBuff + RxLen;
    Pos = RxLen;
    char *pStr = pBuff;
    uint16_t StrLen = Pos;
    pBuff += (Pos + 1);
    Len -= (Pos + 1);

    int16_t RxRssi = atoi(pBuff); 
    
    pPos = strstr(pBuff, ",");
    Pos = pPos - pBuff;
    pBuff += (Pos + 1);
    Len -= (Pos + 1);

    uint16_t RxSnr = atoi(pBuff); 
    
    EventFunc(RxAddress, StrLen, pStr, RxRssi, RxSnr);
  }
}
//==============================================================================


//==============================================================================
// �������-���������� ���� ������� �� ������ (���������� �� ����������� ����������!)
//==============================================================================
static void RxPacket(char *pBuff, uint16_t Len)
{
  // ������� ����� ����
  if (!Len)
    return;
  
  // ��������� ������ ������ (������ ���� "+")
  if (*pBuff == '+')
  {
    pBuff++;
    Len--;
  }
  
  // �������� �����
  if (strstr(pBuff, "RCV="))                    // �������� �����
  {
    RxRFPacket(pBuff, Len);
  }
  else if (strstr(pBuff, "ERR="))               // ������� ������
  {
    pBuff += 4;
    Len -= 4;

    Reyax_Lora_State.ErrorCode = atoi(pBuff);   // ���������� ��� ������
    Reyax_Lora_State.AnswerWaiting = 0;
    Reyax_Lora_State.ErrorReceived = 1;
  }
  else if (Reyax_Lora_State.AnswerWaiting)      // ����� �� ������
  {
    if (strstr(pBuff, Reyax_Lora_State.AnswerWord))
    {
      // ����� �������� �����, ������ ����� ����
      int8_t WordLen = strlen(Reyax_Lora_State.AnswerWord);
      pBuff += WordLen;
      Len -= WordLen;
      
      // ���� "=")
      if ((Len) && (*pBuff == '='))
      {
        pBuff++;
        Len--;
        
        // ���������� �����
        if (Reyax_Lora_State.pAnswerBuff)
          memcpy(Reyax_Lora_State.pAnswerBuff, pBuff, Len);
      }

      Reyax_Lora_State.AnswerWaiting = 0;
      Reyax_Lora_State.AnswerReceived = 1;
    }
  }
}
//==============================================================================


//==============================================================================
// ������� ���������� � ������ pStrBuff ����� ����� Value
//==============================================================================
static int8_t DecToStr(int32_t Value, char* pStrBuff)
{
  uint32_t Divider = 1000000000;
  int8_t Len = 0;
  int8_t Started = 0;
  
  while (Divider)
  {
    // ������� ��������� �����
    uint8_t Digit = (Value / Divider) % 10;
    
    // ���� �� 0, �� ������� ����� � ������
    if ((Digit) || (Started))
    {
      *(pStrBuff++) = Digit + 0x30;
      Len++;
      Started = 1;
    }
    
    Divider /= 10;
  }
  
  *pStrBuff = 0x00;
  return Len;
}
//==============================================================================


//==============================================================================
// ������� ���������� � ������ pStrBuff ���� Value � ����������������� ����
//==============================================================================
static void ByteToHexStr(uint8_t Value, char* pStrBuff)
{
  int8_t Val = Value >> 4;
  
  if (Val < 10)
    *pStrBuff = 0x30 + Val;             // 0-9
  else
    *pStrBuff = 0x41 - 10 + Val;        // A-F
  
  pStrBuff++;
  
  Val = Value & 0x0F;
  
  if (Val < 10)
    *pStrBuff = 0x30 + Val;             // 0-9
  else
    *pStrBuff = 0x41 - 10 + Val;        // A-F
  
  pStrBuff++;
  
  *pStrBuff = 0x00;
}
//==============================================================================


//==============================================================================
// ������� ������ �� ������ pStrBuff ���� Value � ����������������� ����
//==============================================================================
static void HexStrToByte(uint8_t *pValue, char* pStrBuff)
{
  int8_t Value = 0;
  int8_t Val = 0;
  
  if (*pStrBuff < 0x3A)
    Val = *pStrBuff - 0x30;             // 0-9
  else
    Val = *pStrBuff - 0x41 + 10;        // A-F
  
  Value = Val << 4;
  pStrBuff++;
  
  if (*pStrBuff < 0x3A)
    Val = *pStrBuff - 0x30;             // 0-9
  else
    Val = *pStrBuff - 0x41 + 10;        // A-F

  Value |= Val;
  *pValue = Value;
}
//==============================================================================


//==============================================================================
// ������� ������ ������ ������ (��������� �� �����)
//==============================================================================
int8_t Reyax_Lora_SetAddress(uint16_t Address)
{
  SendStr("AT+ADDRESS=");
  
  char Buff[6];
  DecToStr(Address, Buff);
  SendStr(Buff);

  SendStr("\r\n");

  return RecvAnswer("OK", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ ������ ������ (��������� �� �����)
//==============================================================================
int8_t Reyax_Lora_GetAddress(uint16_t *pAddress)
{
  SendStr("AT+ADDRESS?\r\n");

  char Buff[8];
  int8_t err = RecvAnswer("ADDRESS", Buff, 250);
  
  *pAddress = atoi(Buff);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ ID ����
//==============================================================================
int8_t Reyax_Lora_SetNetworkID(uint8_t Id)
{
  if (Id > 16)
    return REYAX_ERR_WRONG_PARAM;
      
  SendStr("AT+NETWORKID=");
  
  char Buff[3];
  DecToStr(Id, Buff);
  SendStr(Buff);
    
  SendStr("\r\n");

  return RecvAnswer("OK", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ ID ����
//==============================================================================
int8_t Reyax_Lora_GetNetworkID(uint8_t *pId)
{
  SendStr("AT+NETWORKID?\r\n");

  char Buff[6];
  int8_t err = RecvAnswer("NETWORKID", Buff, 250);
  
  *pId = atoi(Buff);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ ����������� ������� ������ ������
//==============================================================================
int8_t Reyax_Lora_SetBand(uint32_t Band)
{
  if ((Band < 820000000) || (Band > 1020000000))
    return REYAX_ERR_WRONG_PARAM;
      
  SendStr("AT+BAND=");
  
  char Buff[11];
  DecToStr(Band, Buff);
  SendStr(Buff);
    
  SendStr("\r\n");

  return RecvAnswer("OK", 0, 10);
}
//==============================================================================


//==============================================================================
// ������� ������ ����������� ������� ������ ������
//==============================================================================
int8_t Reyax_Lora_GetBand(uint32_t *pBand)
{
  SendStr("AT+BAND?\r\n");

  char Buff[16];
  int8_t err = RecvAnswer("BAND", Buff, 250);
  
  *pBand = atoi(Buff);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ ����� ������ ���
//==============================================================================
int8_t Reyax_Lora_SetSleepMode(uint8_t SleepModeOn)
{
  if (SleepModeOn > 1)
    return REYAX_ERR_WRONG_PARAM;
      
  SendStr("AT+MODE=");
  
  char Buff[2];
  DecToStr(SleepModeOn, Buff);
  SendStr(Buff);

  SendStr("\r\n");

  return RecvAnswer("OK", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ ����� ������ ���
//==============================================================================
int8_t Reyax_Lora_GetSleepMode(uint8_t *pSleepModeOn)
{
  SendStr("AT+MODE?\r\n");

  char Buff[16];
  int8_t err = RecvAnswer("MODE", Buff, 250);
  
  *pSleepModeOn = atoi(Buff);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ �������� ������ UART ������
//==============================================================================
int8_t Reyax_Lora_SetBaudRate(uint32_t Baud)
{
  if (Baud > 115200)
    return REYAX_ERR_WRONG_PARAM;
      
  SendStr("AT+IPR=");

  char Buff[7];
  DecToStr(Baud, Buff);
  SendStr(Buff);

  SendStr("\r\n");

  return RecvAnswer("OK", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ �������� ������ UART ������
//==============================================================================
int8_t Reyax_Lora_GetBaudRate(uint32_t *pBaud)
{
  SendStr("AT+IPR?\r\n");

  char Buff[8];
  int8_t err = RecvAnswer("IPR", Buff, 250);
  
  *pBaud = atoi(Buff);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ �������� �������� (0-15)
//==============================================================================
int8_t Reyax_Lora_SetPower(uint8_t Power)
{
  if (Power > 15)
    return REYAX_ERR_WRONG_PARAM;
      
  SendStr("AT+CRFOP=");
  
  char Buff[7];
  DecToStr(Power, Buff);
  SendStr(Buff);
    
  SendStr("\r\n");

  return RecvAnswer("OK", 0, 10);
}
//==============================================================================


//==============================================================================
// ������� ������ �������� �������� (0-15)
//==============================================================================
int8_t Reyax_Lora_GetPower(uint8_t *pPower)
{
  SendStr("AT+CRFOP?\r\n");

  char Buff[8];
  int8_t err = RecvAnswer("CRFOP", Buff, 250);
  
  *pPower = atoi(Buff);

  return err;
}
//==============================================================================

  
//==============================================================================
// ������� ������ 32-����������� ����� ���� (AES128) (�� ������ � 16-���� ����)
//==============================================================================
int8_t Reyax_Lora_SetPasswordStr(char *pPassword)
{
  if (strlen(pPassword) != 32)
    return REYAX_ERR_WRONG_PARAM;
  
  SendStr("AT+CPIN=");
  SendStr(pPassword);
  SendStr("\r\n");

  return RecvAnswer("OK", 0, 350);
}
//==============================================================================


//==============================================================================
// ������� ������ 16-�������� ����� ���� (AES128) (�� ������� ����)
//==============================================================================
int8_t Reyax_Lora_SetPasswordBin(uint8_t *pPassword)
{
  for (uint8_t i = 0; i < 16; i++)
    ByteToHexStr(*(pPassword++), &StrBuff[i << 1]);

  return Reyax_Lora_SetPasswordStr(StrBuff);
}
//==============================================================================


//==============================================================================
// ������� ������ 32-����������� ����� ���� (AES128) (� ������ � 16-���� ����)
//==============================================================================
int8_t Reyax_Lora_GetPasswordStr(char *pPassword)
{
  SendStr("AT+CPIN?\r\n");

  char Buff[33];
  int8_t err = RecvAnswer("CPIN", Buff, 250);
  
  strcpy(pPassword, Buff);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ 16-�������� ����� ���� (AES128) (� ������ ����)
//==============================================================================
int8_t Reyax_Lora_GetPasswordBin(uint8_t *pPassword)
{
  char Buff[33];

  int8_t err = Reyax_Lora_GetPasswordStr(Buff);
  if (err != 0)
    return err;
  
  for (uint8_t i = 0; i < 16; i++)
    HexStrToByte(pPassword++, &Buff[i << 1]);

  return err;
}
//==============================================================================


//==============================================================================
// ������� ������ ���������� ������
//==============================================================================
int8_t Reyax_Lora_SetParameters(uint8_t SpreadingFactor, uint8_t Bandwidth, uint8_t CodingRate, uint8_t Preamble)
{
  if ((SpreadingFactor < 7) || (SpreadingFactor > 12))
    return REYAX_ERR_WRONG_PARAM;
  if (Bandwidth > 9)
    return REYAX_ERR_WRONG_PARAM;
  if ((CodingRate < 1) || (CodingRate > 4))
    return REYAX_ERR_WRONG_PARAM;
  if ((Preamble < 4) || (Preamble > 16))
    return REYAX_ERR_WRONG_PARAM;
      
  SendStr("AT+PARAMETER=");
  
  char Buff[3];

  // SpreadingFactor
  DecToStr(SpreadingFactor, Buff);
  SendStr(Buff);
  SendStr(",");
  
  // Bandwidth
  DecToStr(Bandwidth, Buff);
  SendStr(Buff);
  SendStr(",");
  
  // CodingRate
  DecToStr(CodingRate, Buff);
  SendStr(Buff);
  SendStr(",");
  
  // Preamble
  DecToStr(Preamble, Buff);
  SendStr(Buff);
  
  SendStr("\r\n");

  return RecvAnswer("OK", 0, 10);
}
//==============================================================================


//==============================================================================
// ������� ������ ���������� ������
//==============================================================================
int8_t Reyax_Lora_GetParameters(uint8_t *pSpreadingFactor, uint8_t *pBandwidth, uint8_t *pCodingRate, uint8_t *pPreamble)
{
  SendStr("AT+PARAMETER?\r\n");

  char Buff[16];
  char *pBuff = Buff;
  int8_t err = RecvAnswer("PARAMETER", Buff, 250);
  
  *pSpreadingFactor = atoi(pBuff); 

  char *pPos = strstr(pBuff, ",");
  int16_t Pos = pPos - pBuff;
  pBuff += (Pos + 1);
      
  *pBandwidth = atoi(pBuff); 
    
  pPos = strstr(pBuff, ",");
  Pos = pPos - pBuff;
  pBuff += (Pos + 1);

  *pCodingRate = atoi(pBuff); 
    
  pPos = strstr(pBuff, ",");
  Pos = pPos - pBuff;
  pBuff += (Pos + 1);

  *pPreamble = atoi(pBuff); 

  return err;
}
//==============================================================================


//==============================================================================
// ������� �������� ������� �������� ����� �� UART � �������
//==============================================================================
int8_t Reyax_Lora_IsPresent()
{
  SendStr("AT\r\n");
  return RecvAnswer("OK", 0, 10);
}
//==============================================================================


//==============================================================================
// ������� �������� ������� ������ ������
//==============================================================================
int8_t Reyax_Lora_Reset()
{
  SendStr("AT+RESET\r\n");
  return RecvAnswer("READY", 0, 500);
}
//==============================================================================


//==============================================================================
// ������� ������ �������� ������
//==============================================================================
int8_t Reyax_Lora_FactoryReset()
{
  SendStr("AT+FACTORY\r\n");
  return RecvAnswer("FACTORY", 0, 250);
}
//==============================================================================


//==============================================================================
// ������� ������ ������ �������� ������
//==============================================================================
int8_t Reyax_Lora_GetVersion(char *pVersion)
{
  SendStr("AT+VER?\r\n");

  char Buff[33];
  int8_t err = RecvAnswer("VER", Buff, 250);
  
  strcpy(pVersion, Buff);

  return err;
}
//==============================================================================

  
//==============================================================================
// ������� �������� ������� �� �������� ������
//==============================================================================
int8_t Reyax_Lora_Send(uint16_t Address, char *pData)
{
  uint8_t Len = strlen(pData);
  if (Len > 240)
    return REYAX_ERR_WRONG_PARAM;
      
  SendStr("AT+SEND=");
  
  char Buff[5];

  // Address
  DecToStr(Address, Buff);
  SendStr(Buff);
  SendStr(",");
  
  // Len
  DecToStr(Len, Buff);
  SendStr(Buff);
  SendStr(",");
  
  // Data
  UART_Send((uint8_t *)pData, Len);
  
  SendStr("\r\n");

  return RecvAnswer("OK", 0, 300000);// 15000);
}
//==============================================================================


//==============================================================================
// ������������� ���������� � �������
//==============================================================================
void Reyax_Lora_Init(USART_TypeDef* USARTx, uint32_t BaudRate, LoraEventFunc Func)
{
  EventFunc = Func;
  UART_Init(USARTx, BaudRate, RxPacket);
}
//==============================================================================
