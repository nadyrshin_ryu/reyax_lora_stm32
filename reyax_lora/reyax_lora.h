//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#ifndef _REYAX_LORA_H
#define _REYAX_LORA_H

#include <types.h>


// ������������ ���� ������
#define REYAX_ERR_NO_ANSVER   (-3)    // ��� ������ - ���� ����-��� �������������� �� �������
#define REYAX_ERR_NO_RX_BUFF  (-4)    // ��� ������ - �� ������� ��������� �� ������� �����
#define REYAX_ERR_WRONG_PARAM (-5)    // ��� ������ - �� ���������� �������� ��������� �������


// ��� ��������� �� ������� ��������� ��������� ������
typedef void (*LoraEventFunc)(uint16_t RxAddr, uint8_t StrLen, char *pStr, int16_t Rssi, uint16_t Snr);


// ������������� ���������� � �������
void Reyax_Lora_Init(USART_TypeDef* USARTx, uint32_t BaudRate, LoraEventFunc Func);
// ������� ������ �������� ������ UART ������
int8_t Reyax_Lora_SetBaudRate(uint32_t Baud);
// ������� ������ �������� ������ UART ������
int8_t Reyax_Lora_GetBaudRate(uint32_t *pBaud);
// ������� �������� ������� �� �������� ������
int8_t Reyax_Lora_Send(uint16_t Address, char *pData);
// ������� �������� ������� �������� ����� �� UART � �������
int8_t Reyax_Lora_IsPresent();
// ������� ������ ������ �������� ������
int8_t Reyax_Lora_GetVersion(char *pVersion);
// ������� �������� ������� ������ ������
int8_t Reyax_Lora_Reset();
// ������� ������ �������� ������
int8_t Reyax_Lora_FactoryReset();
// ������� ������ ���������� ������
int8_t Reyax_Lora_SetParameters(uint8_t SpreadingFactor, uint8_t Bandwidth, uint8_t CodingRate, uint8_t Preamble);
// ������� ������ ���������� ������
int8_t Reyax_Lora_GetParameters(uint8_t *pSpreadingFactor, uint8_t *pBandwidth, uint8_t *pCodingRate, uint8_t *pPreamble);
// ������� ������ 32-����������� ����� ���� (AES128) (�� ������ � 16-���� ����)
int8_t Reyax_Lora_SetPasswordStr(char *pPassword);
// ������� ������ 16-�������� ����� ���� (AES128) (�� ������� ����)
int8_t Reyax_Lora_SetPasswordBin(uint8_t *pPassword);
// ������� ������ 32-����������� ����� ���� (AES128) (� ������ � 16-���� ����)
int8_t Reyax_Lora_GetPasswordStr(char *pPassword);
// ������� ������ 16-�������� ����� ���� (AES128) (� ������ ����)
int8_t Reyax_Lora_GetPasswordBin(uint8_t *pPassword);
// ������� ������ �������� �������� (0-15)
int8_t Reyax_Lora_SetPower(uint8_t Power);
// ������� ������ �������� �������� (0-15)
int8_t Reyax_Lora_GetPower(uint8_t *pPower);
// ������� ������ ����� ������ ���
int8_t Reyax_Lora_SetSleepMode(uint8_t SleepModeOn);
// ������� ������ ����� ������ ���
int8_t Reyax_Lora_GetSleepMode(uint8_t *pSleepModeOn);
// ������� ������ ����������� ������� ������ ������
int8_t Reyax_Lora_SetBand(uint32_t Band);
// ������� ������ ����������� ������� ������ ������
int8_t Reyax_Lora_GetBand(uint32_t *pBand);
// ������� ������ ������ ������ (��������� �� �����)
int8_t Reyax_Lora_SetAddress(uint16_t Address);
// ������� ������ ������ ������ (��������� �� �����)
int8_t Reyax_Lora_GetAddress(uint16_t *pAddress);
// ������� ������ ID ����
int8_t Reyax_Lora_SetNetworkID(uint8_t Id);
// ������� ������ ID ����
int8_t Reyax_Lora_GetNetworkID(uint8_t *pId);


#endif