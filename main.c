//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x.h>
#include <string.h>
#include <stdio.h>
#include <reyax_lora.h>
#include <hd44780.h>
#include <gpio.h>
#include <uart.h>
#include <timers.h>
#include <delay.h>
#include "main.h"


#define RxMode          0       // 0 - ��� �������� � ��������, 1 - ��� ����������� ��� �������

#if (RxMode)
#define MyAddress       2       // ����� ��������
#else
#define MyAddress       1       // ����� �����������
#endif


//char StrBuff[33] = "FABC0002EEDCAA90FABC0002EEDCAA90";
char StringBuffer[32]; // = "123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";
uint8_t Buff[16] = {0xFA, 0xBC, 0x00, 0x02, 0xEE, 0xDC, 0xAA, 0x90, 0xFA, 0xBC, 0x00, 0x02, 0xEE, 0xDC, 0xAA, 0x90};
char Version[16];
int16_t Err;
uint16_t RxAddress;
int16_t RxRssi;
uint16_t RxSnr;
uint8_t Tmr_Cnt = 0;
int32_t Cnt = 0;
int Result = 0;


// ��������� ��� �������� ������
typedef struct
{
  uint8_t SendFlag      :1;     // 
}
tTimerFlags;

static tTimerFlags Tmr_Flag;


// ������� ���������� �����������
#define LedOff()        GPIO_WriteBit(GPIOB, (1 << 12), Bit_SET)
#define LedOn()         GPIO_WriteBit(GPIOB, 1 << 12, Bit_RESET)


//==============================================================================
// ���������-���������� ������� �� �������. pBuff - ����� � �������� �� ������� 
//==============================================================================
void Lora_Rx(uint16_t RxAddr, uint8_t StrLen, char *pStr, int16_t Rssi, uint16_t Snr)
{
  RxAddress = RxAddr;
  RxRssi = Rssi;
  RxSnr = Snr;
  memcpy(StringBuffer, pStr, StrLen);
  StringBuffer[StrLen] = 0x00;
  
  LedOn();
}
//==============================================================================


//==============================================================================
// ���������, ���������� �� ���������� �������
//==============================================================================
void TimerFunc(void)
{
  if (++Tmr_Cnt > 21)
  {
    Tmr_Cnt = 0;
    Tmr_Flag.SendFlag = 1;
  }

  LedOff();
}
//==============================================================================


//==============================================================================
//
//==============================================================================
void main()
{
  int8_t err = 0;
  
  SystemInit();
  
#if (RxMode)
  hd44780_init();
  hd44780_backlight_set(1);
#else
  gpio_PortClockStart(GPIOA);
  gpio_SetGPIOmode_In(GPIOA, 1 << 15, gpio_PullUp);
#endif    
  
  gpio_PortClockStart(GPIOB);
  gpio_SetGPIOmode_Out(GPIOB, 1 << 12);

  Reyax_Lora_Init(USART1, 115200, Lora_Rx);

  err = Reyax_Lora_IsPresent();

  //err = Reyax_Lora_FactoryReset();
  //err = Reyax_Lora_Reset();
  //err = Reyax_Lora_SetSleepMode(0);

  uint8_t SpreadingFactor = 0;
  uint8_t Bandwidth = 0;
  uint8_t CodingRate = 0;
  uint8_t Preamble = 0;
  err = Reyax_Lora_GetParameters(&SpreadingFactor, &Bandwidth, &CodingRate, &Preamble);

  err = Reyax_Lora_SetAddress(MyAddress);
  err = Reyax_Lora_SetNetworkID(1);
  err = Reyax_Lora_SetPasswordBin(Buff);
  err = Reyax_Lora_SetBand(868000000);

  //err = Reyax_Lora_SetParameters(12, 7, 1, 4);        // �� ���������� ���� ����� ���������
  err = Reyax_Lora_SetParameters(12, 7, 4, 4);

  // ���� ��� ������� ������ �� ������
  char StrBuff[33];
  err = Reyax_Lora_GetPasswordStr(StrBuff);
  memset(Buff, 0, 16);
  err = Reyax_Lora_GetPasswordBin(Buff);
  uint32_t Baud = 0;
  err = Reyax_Lora_GetBaudRate(&Baud);
  uint8_t Power = 0;
  err = Reyax_Lora_GetPower(&Power);
  uint32_t Band = 0;
  err = Reyax_Lora_GetBand(&Band);
  uint16_t Address = 0;
  err = Reyax_Lora_GetAddress(&Address);
  uint8_t Id = 0;
  err = Reyax_Lora_GetNetworkID(&Id);
  uint8_t SleepModeOn = 0;
  err = Reyax_Lora_GetSleepMode(&SleepModeOn);
  
#if (RxMode)
  err = Reyax_Lora_SetPower(10);

  hd44780_goto_xy(0, 0);
  hd44780_printf("Module FW VER:");
  err = Reyax_Lora_GetVersion(Version);
  hd44780_goto_xy(1, 0);
  hd44780_printf(Version);
  delay_ms(3000);
  hd44780_clear();
  memset(StringBuffer, 0, sizeof(StringBuffer));
#else
  // ������������� �������� ����������� � ����������� �� ������� ��������� ����� A15 � GND
  if (GPIO_ReadInputDataBit(GPIOA, (1 << 15)))
    err = Reyax_Lora_SetPower(10);      // ��� ���������
  else
    err = Reyax_Lora_SetPower(15);      // ���� ���������
#endif    
  
  tmr2_init(10, TimerFunc);
  tmr2_start();
  
  while (1)
  {
#if (RxMode)    // ��� ��� ��������
    // ����� �� �������
    hd44780_goto_xy(0, 0);
    hd44780_printf("%d: %s ", RxAddress, StringBuffer);
    hd44780_goto_xy(1, 0);
    hd44780_printf("RSSI=%d SNR=%d ", RxRssi, RxSnr);
    delay_ms(100);
#else           // ��� ��� �����������
    if (Tmr_Flag.SendFlag)
    {
      Tmr_Flag.SendFlag = 0;

      LedOn();

      // �������� ������ ��������
      int32_t n = sprintf(StringBuffer, "Test %d", Cnt++);
      err = Reyax_Lora_Send(2, StringBuffer);
    }
#endif
  }
}
//==============================================================================
